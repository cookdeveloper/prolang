#ifndef PROLANG_STRINGS_H
#define PROLANG_STRINGS_H

#include "base/types.h"

namespace base {

enum class EStringId {
// MENU BAR
// MENU BAR: FILE
  MENU_FILE,
  MENU_FILE_NEW,
  MENU_FILE_OPEN,
  MENU_FILE_SAVE,
  MENU_FILE_SAVE_ALL,
  MENU_FILE_CLOSE,
  MENU_FILE_CLOSE_ALL,
  MENU_FILE_QUIT,
// MENU BAR: HELP
  MENU_HELP,
  MENU_HELP_ABOUT,
};

String getString(EStringId stringId);

} // namespace base

#endif // PROLANG_STRINGS_H
