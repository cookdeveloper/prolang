#ifndef PROLANG_TYPES_H
#define PROLANG_TYPES_H

#include <map>
#include <set>
#include <vector>

#include <QObject>
#include <QString>
#include <QVariant>

namespace base {

// Basic
using String = QString;
using Integer = int;
using Unsigned = uint;
using Real = double;
using Variant = QVariant;

// Containers
template <typename K, typename V>
using Map = std::map<K, V>;

template<typename T>
using Vector = std::vector<T>;

template<typename T>
using Set = std::set<T>;

// GUI

// Domain
using WireValue = Variant;
using WireIndex = Unsigned;

} // namespace base

#endif // PROLANG_TYPES_H
