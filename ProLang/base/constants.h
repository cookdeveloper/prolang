#ifndef PROLANG_CONSTANTS_H
#define PROLANG_CONSTANTS_H

#include "base/types.h"

namespace base {

constexpr Integer CANVAS_MINIMAL_WIDTH = 500;
constexpr Integer CANVAS_MINIMAL_HEIGHT = 500;
const String CONTEXT_WORLD_NAME = ">";

} // namespace base

#endif // PROLANG_CONSTANTS_H
