#include "strings.h"

namespace base {

String getString(EStringId stringId) {

  static Map<EStringId, String> stringIdToStringMap {
    { EStringId::MENU_FILE, QObject::tr("&File") },
    { EStringId::MENU_FILE_NEW, QObject::tr("&New") },
    { EStringId::MENU_FILE_OPEN, QObject::tr("&Open") },
    { EStringId::MENU_FILE_SAVE, QObject::tr("&Save") },
    { EStringId::MENU_FILE_SAVE_ALL, QObject::tr("Save All") },
    { EStringId::MENU_FILE_CLOSE, QObject::tr("Close") },
    { EStringId::MENU_FILE_CLOSE_ALL, QObject::tr("Close All") },
    { EStringId::MENU_FILE_QUIT, QObject::tr("&Quit") },
    { EStringId::MENU_HELP, QObject::tr("&Help") },
    { EStringId::MENU_HELP_ABOUT, QObject::tr("About") },
  };

  assert(stringIdToStringMap.find(stringId) != std::cend(stringIdToStringMap));

  return stringIdToStringMap[stringId];
}

} // namespace base
