#include "main_window.h"

#include <QMdiArea>
#include <QMdiSubWindow>
#include <QMenuBar>

#include "base/strings.h"
#include "gui/canvas.h"

namespace gui {

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent) {
  initMenuBar();
  initCentralWidget();
}


MainWindow::~MainWindow() {}


void MainWindow::initMenuBar() {
  QAction* const fileNewAction = new QAction(base::getString(base::EStringId::MENU_FILE_NEW), this);
  QAction* const fileOpenAction = new QAction(base::getString(base::EStringId::MENU_FILE_OPEN), this);
  QAction* const fileSaveAction = new QAction(base::getString(base::EStringId::MENU_FILE_SAVE), this);
  QAction* const fileSaveAllAction = new QAction(base::getString(base::EStringId::MENU_FILE_SAVE_ALL), this);
  QAction* const fileCloseAction = new QAction(base::getString(base::EStringId::MENU_FILE_CLOSE), this);
  QAction* const fileCloseAllAction = new QAction(base::getString(base::EStringId::MENU_FILE_CLOSE_ALL), this);
  QAction* const fileQuitAction = new QAction(base::getString(base::EStringId::MENU_FILE_QUIT), this);

  QAction* const helpAboutAction = new QAction(base::getString(base::EStringId::MENU_HELP_ABOUT), this);

  QMenu* const fileMenu = menuBar()->addMenu(base::getString(base::EStringId::MENU_FILE));
  fileMenu->addAction(fileNewAction);
  fileMenu->addAction(fileOpenAction);
  fileMenu->addAction(fileSaveAction);
  fileMenu->addAction(fileSaveAllAction);
  fileMenu->addSeparator();
  fileMenu->addAction(fileCloseAction);
  fileMenu->addAction(fileCloseAllAction);
  fileMenu->addSeparator();
  fileMenu->addAction(fileQuitAction);

  QMenu* const helpMenu = menuBar()->addMenu(base::getString(base::EStringId::MENU_HELP));
  helpMenu->addAction(helpAboutAction);

  connect(fileNewAction, &QAction::triggered, this, &MainWindow::newFileActionTriggered);
  connect(fileOpenAction, &QAction::triggered, this, &MainWindow::openFileActionTriggered);
  connect(fileSaveAction, &QAction::triggered, this, &MainWindow::saveFileActionTriggered);
  connect(fileSaveAllAction, &QAction::triggered, this, &MainWindow::saveAllFilesActionTriggered);
  connect(fileCloseAction, &QAction::triggered, this, &MainWindow::closeFileActionTriggered);
  connect(fileCloseAllAction, &QAction::triggered, this, &MainWindow::closeAllFilesActionTriggered);
  connect(fileQuitAction, &QAction::triggered, this, &MainWindow::quitActionTriggered);

  connect(helpAboutAction, &QAction::triggered, this, &MainWindow::aboutActionTriggered);
}


void MainWindow::initCentralWidget() {
  setCentralWidget(new QMdiArea(this));
}


void MainWindow::newFileActionTriggered() {
  if (QMdiArea* const mdi = dynamic_cast<QMdiArea*>(centralWidget())) {
    QMdiSubWindow* const sub = new QMdiSubWindow(this);
    sub->setWidget(new Canvas(this));
    sub->setAttribute(Qt::WA_DeleteOnClose);
    mdi->addSubWindow(sub);
    sub->showMaximized();
  }
}


void MainWindow::openFileActionTriggered() {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
}


void MainWindow::saveFileActionTriggered() {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
}


void MainWindow::saveAllFilesActionTriggered() {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
}


void MainWindow::closeFileActionTriggered() {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
}


void MainWindow::closeAllFilesActionTriggered() {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
}


void MainWindow::quitActionTriggered() {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
}


void MainWindow::aboutActionTriggered() {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
}

} // namespace gui
