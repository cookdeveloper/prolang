#ifndef PROLANG_PL_MAIN_WINDOW_H
#define PROLANG_PL_MAIN_WINDOW_H

#include <QMainWindow>

namespace gui {

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void newFileActionTriggered();
    void openFileActionTriggered();
    void saveFileActionTriggered();
    void saveAllFilesActionTriggered();
    void closeFileActionTriggered();
    void closeAllFilesActionTriggered();
    void quitActionTriggered();

    void aboutActionTriggered();

private:
    void initMenuBar();
    void initCentralWidget();

};

} // namespace gui

#endif // PROLANG_PL_MAIN_WINDOW_H
