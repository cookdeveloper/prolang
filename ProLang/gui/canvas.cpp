#include "canvas.h"

#include <QDebug>
#include <QPainter>
#include <QPaintEvent>

#include "base/constants.h"
#include "core/functions.h"
#include "core/process.h"
#include "core/process_geometry.h"
#include "core/processes.h"
#include "core/wire.h"
#include "core/wire_geometry.h"

namespace gui {

Canvas::Canvas(QWidget* parent)
  : QWidget(parent) {

  setMinimumSize(QSize(base::CANVAS_MINIMAL_WIDTH, base::CANVAS_MINIMAL_HEIGHT));
  setMouseTracking(true);
  setAutoFillBackground(true);

  initWorld();
  initContext();
  initStaticGraphics();
}


void Canvas::paintEvent(QPaintEvent* event) {
  QPainter p(this);
  p.setRenderHint(QPainter::Antialiasing);
  p.fillRect(rect(), graphics_.color_background);

  // CONTEXT PROCESS
  p.setPen(QPen(graphics_.color_process, graphics_.width_of_process_pen));
  p.fillRect(graphics_.context_rect, graphics_.color_context_background);

  //    Name
  p.setFont(graphics_.font);
  p.setPen(QPen(graphics_.color_context, graphics_.width_of_context_pen));
  p.drawText(graphics_.context_name_coordinates, context_->getName());

  // INPUTS
  QPointF input_context_connection_point = graphics_.input_connections_start;
  QRectF input_rect;
  for (const core::Connection* const kInputConnection : context_->getInputs()) {
    if (kInputConnection) {
      input_context_connection_point.setX(input_context_connection_point.x() + graphics_.input_connections_step);
      input_rect = QRectF(input_context_connection_point, QSize(50, graphics_.input_connections_height));
      p.fillRect(input_rect, graphics_.color_context_background);
      p.drawText(input_context_connection_point + QPoint(0, graphics_.input_connections_height / 2), kInputConnection->getName());
    }
  }

  // OUTPUTS
  QPointF output_connection_point = graphics_.output_connections_start;
  QRectF output_rect;
  for (const core::Connection* const kOutputConnection : context_->getOutputs()) {
    if (kOutputConnection) {
      output_connection_point.setX(output_connection_point.x() + graphics_.output_connections_step);
      output_rect = QRectF(output_connection_point, QSize(50, graphics_.output_connections_height));
      p.fillRect(output_rect, graphics_.color_context_background);
      p.drawText(output_connection_point + QPoint(0, graphics_.output_connections_height / 2), kOutputConnection->getName());
    }
  }

  // CHILD PROCESSES
  if (const auto kChildren = processes_hierarchy_.find(context_);
      kChildren != std::cend(processes_hierarchy_)) {

    for (const core::Process* const kChild : kChildren->second) {

      const QRectF& kRect = fromRelativeCoordinates(kChild->getGeometry()->getRect());
      p.drawRect(kRect);
      p.drawText(kRect, kChild->getName());

      const base::Integer kConnectionRectSide = calculateConnectionsHeight(kChild);

      // Input connections
      QPointF input_connection_point = calculateInputConnectionsStart(kChild);
      QRectF input_pect;
      for (const core::Connection* kInputConnection : kChild->getInputs()) {
        if (kInputConnection) {
          input_connection_point.setX(input_connection_point.x() + calculateInputConnectionsStep(kChild));
          input_pect = QRectF(input_connection_point, QSizeF(kConnectionRectSide, kConnectionRectSide));
          p.fillRect(input_pect, graphics_.color_process);
        }
      }

      // Output connections
      QPointF output_connection_point = calculateOutputConnectionsStart(kChild);
      QRectF output_rect;
      for (const core::Connection* const kOutputConnection : kChild->getOutputs()) {
        if (kOutputConnection) {
          output_connection_point.setX(output_connection_point.x() + calculateOutputConnectionsStep(kChild));
          output_connection_point.setY(output_connection_point.y() - kConnectionRectSide);
          output_rect = QRectF(output_connection_point, QSizeF(kConnectionRectSide, kConnectionRectSide));
          p.fillRect(output_rect, graphics_.color_process);
        }
      }
    }
  }

  BaseClass::paintEvent(event);
}


void Canvas::resizeEvent(QResizeEvent* event) {
  initDynamicGraphics();
  BaseClass::resizeEvent(event);
}


void Canvas::mouseMoveEvent(QMouseEvent* event) {
//  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
  BaseClass::mouseMoveEvent(event);
}


void Canvas::mousePressEvent(QMouseEvent* event) {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
  BaseClass::mousePressEvent(event);
}


void Canvas::mouseReleaseEvent(QMouseEvent* event) {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
  BaseClass::mouseReleaseEvent(event);
}


void Canvas::mouseDoubleClickEvent(QMouseEvent* event) {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
  BaseClass::mouseDoubleClickEvent(event);
}


core::Process* Canvas::makeTestProcess() {

  core::Process* const p = new core::Process("myProcess", false, nullptr, this);
  registrateProcess(p);
  core::Wire* const world_in1 = new core::Wire(core::WireType::NUMBER_REAL);
  core::Wire* const world_in2 = new core::Wire(core::WireType::NUMBER_REAL);
  core::Wire* const world_in3 = new core::Wire(core::WireType::NUMBER_REAL);
  core::Wire* const world_output = new core::Wire(core::WireType::NUMBER_REAL);
  p->addInput("a", core::WireType::NUMBER_REAL);
  p->addInput("b", core::WireType::NUMBER_REAL);
  p->addInput("c", core::WireType::NUMBER_REAL);

  core::Wire* const result = new core::Wire(core::WireType::NUMBER_REAL);
  p->addOutput("result", core::WireType::NUMBER_REAL);

  core::Wire* const in1 = new core::Wire(core::WireType::NUMBER_REAL);
  core::Wire* const in2 = new core::Wire(core::WireType::NUMBER_REAL);
  core::Wire* const in3 = new core::Wire(core::WireType::NUMBER_REAL);

  p->getInput(0)->connectWire(world_in1, in1);
  p->getInput(1)->connectWire(world_in2, in2);
  p->getInput(2)->connectWire(world_in3, in3);

  p->getOutput(0)->connectWire(result, world_output);

  core::Process* const plus = core::process::addition(core::WireType::NUMBER_REAL, p, p);
  registrateProcess(plus);
  plus->getGeometry()->setRect(QRectF(0.2, 0.6, 0.6, 0.2));
  plus->getInput(0)->connectWire(in1);
  plus->getInput(1)->connectWire(in2);

  core::Wire* const plusOut = new core::Wire(core::WireType::NUMBER_REAL);
  plus->getOutput(0)->connectWire(plusOut);

  core::Process* const mult = core::process::multiplication(core::WireType::NUMBER_REAL, p, p);
  registrateProcess(mult);
  mult->getGeometry()->setRect(QRectF(0.2, 0.2, 0.6, 0.2));
  mult->getInput(0)->connectWire(plusOut);
  mult->getInput(1)->connectWire(in3);

  mult->getOutput(0)->connectWire(result);

  world_in1->send(base::WireValue(30.0));
  world_in2->send(base::WireValue(3.0));
  world_in3->send(base::WireValue(2.0));

  qDebug() << "OUTPUT:" << result->get().toReal();
  return p;
}


void Canvas::initStaticGraphics() {
  qDebug() << __FILE__ << __FUNCTION__;
  graphics_.font = QFont("Courier", 30);
  graphics_.color_background = QColor(150, 150, 150);
  graphics_.color_context_background = QColor(200, 200, 200);
  graphics_.color_context = QColor(25, 25, 25);
  graphics_.color_process = QColor(100, 100, 100);
  graphics_.width_of_context_pen = 1;
  graphics_.width_of_process_pen = 1;
  graphics_.round_of_process_frame = 5;
}


void Canvas::initDynamicGraphics() {

  const base::Integer kW = width();
  const base::Integer kH = height();

  graphics_.context_rect = QRect(kW * 0.05,
                                 kH * 0.1,
                                 kW * 0.9,
                                 kH * 0.85);

  graphics_.context_name_coordinates = graphics_.context_rect.topLeft() + QPoint(10, 20);

  graphics_.input_connections_start = calculateInputConnectionsStart(context_, kW, kH);
  graphics_.input_connections_step = calculateInputConnectionsStep(context_, kW);
  graphics_.input_connections_height = calculateConnectionsHeight(context_, kH);

  graphics_.output_connections_start = calculateOutputConnectionsStart(context_, kW, kH);
  graphics_.output_connections_step = calculateOutputConnectionsStep(context_, kW);
  graphics_.output_connections_height = graphics_.input_connections_height;
}


void Canvas::initWorld() {
  //world_ = new core::Process(base::CONTEXT_WORLD_NAME, this);
  world_ = makeTestProcess(); // test
}


void Canvas::initContext() {
  if (world_) {
    context_ = world_;
  }
}


void Canvas::registrateProcess(core::Process* p) {
  if (p->getParentProcess()) {
    processes_hierarchy_[p->getParentProcess()].insert(p);
  }
  else {
    processes_hierarchy_[p] = base::Set<core::Process*>();
  }
}


QRectF Canvas::fromRelativeCoordinates(const QRectF& r) {
  const QRect& kContext = graphics_.context_rect;
  return QRectF(QPointF(kContext.x() + r.x() * kContext.width(),
                        kContext.y() + r.y() * kContext.height()),
                QSizeF(r.width() * kContext.width(),
                       r.height() * kContext.height()));
}


QPointF Canvas::calculateInputConnectionsStart(const core::Process* process,
                                               base::Integer width,
                                               base::Integer height) {
  if (process != context_) {
    return fromRelativeCoordinates(process->getGeometry()->getRect()).bottomLeft() +
           QPointF(graphics_.context_rect.x(), 0.0);
  }
  else {
    return QPointF(width * 0.1,
                   height * 0.95);
  }
}

base::Integer Canvas::calculateInputConnectionsStep(const core::Process* process,
                                                    base::Integer width) {
  if (process != context_) {
    return fromRelativeCoordinates(process->getGeometry()->getRect()).width() * 0.8 /
           (process->getInputs().size() + 1);
  }
  else {
    return width * 0.8 /
           (context_->getInputs().size() + 1);
  }
}

base::Integer Canvas::calculateConnectionsHeight(const core::Process* process,
                                                 base::Integer height) {
  if (process != context_) {
    return fromRelativeCoordinates(process->getGeometry()->getRect()).height() * 0.05;
  }
  else {
    return height * 0.05;
  }
}

QPointF Canvas::calculateOutputConnectionsStart(const core::Process* process,
                                                base::Integer width,
                                                base::Integer height) {
  if (process != context_) {
    return fromRelativeCoordinates(process->getGeometry()->getRect()).topLeft() +
           QPointF(graphics_.context_rect.x(), 0.0);
  }
  else {
    return QPointF(width * 0.1,
                   height * 0.05);
  }
}

base::Integer Canvas::calculateOutputConnectionsStep(const core::Process* process,
                                                     base::Integer width) {
  if (process != context_) {
    return fromRelativeCoordinates(process->getGeometry()->getRect()).width() * 0.8 /
           (process->getOutputs().size() + 1);
  }
  else {
    return width * 0.8 /
           (context_->getOutputs().size() + 1);
  }
}

} // namespace gui
