#ifndef PROLANG_CANVAS_H
#define PROLANG_CANVAS_H

#include <QPen>
#include <QWidget>

#include "base/types.h"

namespace core {
class Process;
}

namespace gui {

class Canvas : public QWidget {
  Q_OBJECT
  using BaseClass = QWidget;
public:
  explicit Canvas(QWidget* parent = nullptr);

protected:
  virtual void paintEvent(QPaintEvent* event);
  virtual void resizeEvent(QResizeEvent* event);
  virtual void mouseMoveEvent(QMouseEvent* event);
  virtual void mousePressEvent(QMouseEvent* event);
  virtual void mouseReleaseEvent(QMouseEvent* event);
  virtual void mouseDoubleClickEvent(QMouseEvent* event);

private:
  core::Process* makeTestProcess();
  void initStaticGraphics();
  void initDynamicGraphics();
  void initWorld();
  void initContext();
  void registrateProcess(core::Process* p);
  QRectF fromRelativeCoordinates(const QRectF& r);
  QPointF calculateInputConnectionsStart(const core::Process* process,
                                         base::Integer width = 0,
                                         base::Integer height = 0);
  base::Integer calculateInputConnectionsStep(const core::Process* process,
                                              base::Integer width = 0);
  base::Integer calculateConnectionsHeight(const core::Process* process,
                                           base::Integer height = 0);
  QPointF calculateOutputConnectionsStart(const core::Process* process,
                                          base::Integer width = 0,
                                          base::Integer height = 0);
  base::Integer calculateOutputConnectionsStep(const core::Process* process,
                                               base::Integer width = 0);

private:
  core::Process* context_ {};
  core::Process* world_ {};
  base::Map<core::Process*, base::Set<core::Process*>> processes_hierarchy_ {};

  struct Graphics {
    QFont font;
    QColor color_background;
    QColor color_context_background;
    QColor color_context;
    QColor color_process;
    base::Integer width_of_context_pen;
    base::Integer width_of_process_pen;
    base::Integer round_of_process_frame;
    QPoint context_name_coordinates;
    QRect context_rect;
    QPointF input_connections_start;
    QPointF output_connections_start;
    base::Integer input_connections_step;
    base::Integer input_connections_height;
    base::Integer output_connections_step;
    base::Integer output_connections_height;
  } graphics_;
};

} // namespace gui

#endif // PROLANG_CANVAS_H
