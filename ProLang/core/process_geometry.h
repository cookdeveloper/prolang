#ifndef PROLANG_PROCESSGEOMETRY_H
#define PROLANG_PROCESSGEOMETRY_H

#include "core/geometry.h"

#include <QRectF>

namespace core {

class ProcessGeometry : public Geometry {
  Q_OBJECT
  using BaseClass = Geometry;
public:
  explicit ProcessGeometry(QObject* parent = nullptr);
  virtual ~ProcessGeometry();

  QRectF getRect() const { return rect_; }
  void setRect(const QRectF& rect);

private:
  QRectF rect_ {};
};

} // namespace core

#endif // PROLANG_PROCESSGEOMETRY_H
