#ifndef PROLANG_FUNCTIONS_H
#define PROLANG_FUNCTIONS_H

#include "core/process.h"

namespace core::function {

// ARITHMETICS
Process::Function addition();
Process::Function subtraction();
Process::Function multiplication();
Process::Function division();

} // namespace core

#endif // PROLANG_FUNCTIONS_H
