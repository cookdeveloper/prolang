#ifndef PROLANG_WIREGEOMETRY_H
#define PROLANG_WIREGEOMETRY_H

#include "core/geometry.h"

#include <QLineF>

namespace core {

class WireGeometry : public Geometry {
  Q_OBJECT
  using BaseClass = Geometry;
public:
  explicit WireGeometry(QObject* parent = nullptr);
  virtual ~WireGeometry();

  QPointF getStartNode() const { return line_.p1(); }
  QPointF getEndNode() const { return line_.p2(); }

  void setStartNode(const QPointF& p);
  void setEndNode(const QPointF& p);

private:
  void updateBoundingRect();

private:
  QLineF line_ {};
};

} // namespace core

#endif // PROLANG_WIREGEOMETRY_H
