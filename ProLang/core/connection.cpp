#include "core/connection.h"

#include "core/wire.h"

#include <QDebug>

namespace core {

Connection::Connection(const base::String& name,
                       EMode mode,
                       Process* process,
                       WireType type,
                       QObject* parent)
  : BaseClass(parent),
    name_(name),
    mode_(mode),
    type_(type),
    process_(process)
{}


void Connection::connectWire(Wire* wire_from, Wire* wire_to) {
  assert(wire_from != nullptr);

  wire_from_ = wire_from;
  wire_to_ = wire_to;

  connect(wire_from_, &Wire::activated, this, [this](){
    is_ready_ = true;
    emit activated();
  });
}


void Connection::sendFurther() {
  if (wire_to_) {
    wire_to_->send(wire_from_->get());
  }
}

} // namespace core
