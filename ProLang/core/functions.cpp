#include "core/functions.h"

#include "base/types.h"
#include "core/wire.h"

namespace core::function {

Process::Function addition() {
  return [](core::Process* p) {

    base::Real res {0};
    for (const core::Connection* const c : p->getInputs()) {
      if (const core::Wire* const w = c->getWireFrom()) {
         res += w->get().toReal();
      }
    }

    if (const core::Connection* const outputConnection = p->getOutput(0)) {
      if (core::Wire* const outputWire = outputConnection->getWireFrom()) {
        outputWire->send(res);
      }
    }
  };
}


Process::Function subtraction() {
  return [](core::Process* p) {

    const base::Unsigned inputsCount = p->getInputs().size();
    assert(inputsCount > 0);

    base::Real res {};
    const core::Connection* const firstConnection = p->getInputs().at(0);
    if (firstConnection) {
      if (const core::Wire* const firstWire = firstConnection->getWireFrom()) {
        res = firstWire->get().toReal();
      }
    }

    if (inputsCount == 1) {
      res = -res;
    }
    else {
      for (const core::Connection* const c : p->getInputs()) {
        if (const core::Wire* const w = c->getWireFrom()) {
          if (c != firstConnection) {
            res -= w->get().toReal();
          }
        }
      }
    }

    if (const core::Connection* const outputConnection = p->getOutput(0)) {
      if (core::Wire* const outputWire = outputConnection->getWireFrom()) {
        outputWire->send(res);
      }
    }
  };
}


Process::Function multiplication() {
  return [](core::Process* p) {

    base::Real res {1};
    for (const core::Connection* const c : p->getInputs()) {
      if (const core::Wire* const w = c->getWireFrom()) {
        res *= w->get().toReal();
      }
    }

    if (const core::Connection* const outputConnection = p->getOutput(0)) {
      if (core::Wire* const outputWire = outputConnection->getWireFrom()) {
        outputWire->send(res);
      }
    }
  };
}


Process::Function division() {
  return [](core::Process* p) {

    base::Real res {};
    const core::Connection* const firstConnection = p->getInputs().at(0);
    if (firstConnection) {
      if (const core::Wire* const firstWire = firstConnection->getWireFrom()) {
        res = firstWire->get().toReal();
      }
    }

    for (const core::Connection* const c : p->getInputs()) {
      if (const core::Wire* const w = c->getWireFrom()) {
        if (c != firstConnection) {
          res /= w->get().toReal();
        }
      }
    }

    if (const core::Connection* const outputConnection = p->getOutput(0)) {
      if (core::Wire* const outputWire = outputConnection->getWireFrom()) {
        outputWire->send(res);
      }
    }
  };
}


} // namespace core
