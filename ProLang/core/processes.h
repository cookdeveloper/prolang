#ifndef PROLANG_PROCESSES_H
#define PROLANG_PROCESSES_H

#include "core/process.h"

namespace core::process {

Process* addition(WireType type, Process* parent_process, QObject* parent = nullptr);
Process* subtraction(WireType type, Process* parent_process, QObject* parent = nullptr);
Process* multiplication(WireType type, Process* parent_process, QObject* parent = nullptr);
Process* division(WireType type, Process* parent_process, QObject* parent = nullptr);

} // namespace core::process

#endif // PROLANG_PROCESSES_H
