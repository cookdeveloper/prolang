#ifndef PROLANG_WIRE_H
#define PROLANG_WIRE_H

#include <QObject>

#include "base/types.h"
#include "core/wire_geometry.h"
#include "core/wire_types.h"

namespace core {

class Process;

class Wire : public QObject {
  Q_OBJECT
public:
  explicit Wire(WireType type,
                QObject* parent = nullptr);

  void send(const base::WireValue& value);
  base::WireValue get() const;

  WireGeometry* getGeometry() { return geometry_; }

signals:
  void activated();

private:
  WireGeometry* geometry_ {};
  WireType type_ {};
  base::WireValue value_ {};
  Process* from_ {};
  Process* to_ {};
};

} // core

#endif // PROLANG_WIRE_H
