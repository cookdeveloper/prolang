#include "core/wire_geometry.h"

#include <QDebug>

namespace core {

WireGeometry::WireGeometry(QObject* parent)
  : Geometry(parent) {

}


WireGeometry::~WireGeometry() {}


void WireGeometry::setStartNode(const QPointF& p) {
  line_.setP1(p);
  updateBoundingRect();
}


void WireGeometry::setEndNode(const QPointF& p) {
  line_.setP2(p);
  updateBoundingRect();
}


void WireGeometry::updateBoundingRect() {
  qDebug() << "TODO: implement it!" << __FILE__ << __FUNCTION__;
}

} // namespace core
