#ifndef PROLANG_WIRE_TYPES_H
#define PROLANG_WIRE_TYPES_H

namespace core {

enum class WireType {
  NUMBER,
  NUMBER_INTEGER,
  NUMBER_REAL,
};

} // namespace core

#endif // PROLANG_WIRE_TYPES_H
