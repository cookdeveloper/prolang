#include "core/processes.h"

#include "core/functions.h"

namespace core::process {

Process* addition(WireType type, Process* parent_process, QObject* parent) {

  Process* const p = new Process("+", true, parent_process, parent);
  p->setFunction(function::addition());

  p->addInput("i_1", type);
  p->addInput("i_2", type);

  p->addOutput("o_1", type);

  return p;
}


Process* subtraction(WireType type, Process* parent_process, QObject* parent) {

  Process* const p = new Process("-", true, parent_process, parent);
  p->setFunction(function::subtraction());

  p->addInput("i_1", type);
  p->addInput("i_2", type);

  p->addOutput("o_1", type);

  return p;
}


Process* multiplication(WireType type, Process* parent_process, QObject* parent) {

  Process* const p = new Process("*", true, parent_process, parent);
  p->setFunction(function::multiplication());

  p->addInput("i_1", type);
  p->addInput("i_2", type);

  p->addOutput("o_1", type);

  return p;
}


Process* division(WireType type, Process* parent_process, QObject* parent) {

  Process* const p = new Process("/", true, parent_process, parent);
  p->setFunction(function::division());

  p->addInput("i_1", type);
  p->addInput("i_2", type);

  p->addOutput("o_1", type);

  return p;
}


} // namespace core::process
