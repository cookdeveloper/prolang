#ifndef PROLANG_CONNECTION_H
#define PROLANG_CONNECTION_H

#include <QObject>

#include "base/types.h"
#include "core/wire_types.h"

namespace core {

class Process;
class Wire;

class Connection : public QObject {
  Q_OBJECT
  using BaseClass = QObject;

public:
  enum class EMode {
    INPUT,
    OUTPUT
  };

  explicit Connection(const base::String& name,
                      EMode mode,
                      Process* process,
                      WireType type,
                      QObject* parent = nullptr);

  base::String getName() const { return name_; }
  Wire* getWireFrom() const { return wire_from_; }
  Wire* getWireTo() const { return wire_to_; }
  bool isReady() const { return is_ready_; }

  void connectWire(Wire* wire_from, Wire* wire_to = nullptr);
  void sendFurther();

signals:
  void activated();

private:
  base::String name_ {};
  EMode mode_ {};
  WireType type_ {};
  Process* process_ {};
  Wire* wire_from_ {};
  Wire* wire_to_ {};
  bool is_ready_ {};
};

} //namespace core

#endif // PROLANG_CONNECTION_H
