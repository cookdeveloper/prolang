#include "core/process.h"

#include "core/wire.h"

namespace core {

Process::Process(const base::String& name,
                 bool atomic,
                 Process* parent_process,
                 QObject* parent)
  : BaseClass(parent),
    parent_process_(parent_process),
    is_atomic_(atomic),
    name_(name) {
  qDebug() << "Process::Process(" << name << ")";
  geometry_ = new ProcessGeometry(this);
}


Process::~Process() {
  qDebug() << "Process::~Process(" << name_ << ")";
}


void Process::addConnection(Connection::EMode mode,
                            const base::String& name,
                            WireType type) {

  Connection* const connection = new Connection(name, mode, this, type, this);
  if (mode == Connection::EMode::INPUT) {
    inputs_.push_back(connection);
    connect(connection, &Connection::activated, this, &Process::tryToCalculate);
  }
  else {
    outputs_.push_back(connection);
  }
}


void Process::addInput(const base::String& name,
                       WireType type) {
  qDebug() << "Process::addInput";
  addConnection(Connection::EMode::INPUT, name, type);
}


void Process::addOutput(const base::String& name, WireType type) {
  qDebug() << "Process::addOutput";
  addConnection(Connection::EMode::OUTPUT, name, type);
}


Connection* Process::getInput(base::WireIndex index) const {
  assert(index < inputs_.size());
  return inputs_.at(index);
}


Connection* Process::getOutput(base::WireIndex index) const {
  assert(index < outputs_.size());
  return outputs_.at(index);
}


void Process::calculate() {
  qDebug() << "Process::calculate" << getName();
  if (isAtomic()) {
    function_(this);
  }
  else {
    for (Connection* c : getInputs()) {
      c->sendFurther();
    }
  }
}


void Process::tryToCalculate() {
  qDebug() << "Process::tryToCalculate" << getName();
  const std::vector<Connection*>& inputs = getInputs();
  if (std::all_of(std::cbegin(inputs),
                  std::cend(inputs),
                  [&](Connection* c){ return c->isReady(); })) {
    calculate();
  }
}

} // namespace core
