#ifndef PROLANG_PROCESS_H
#define PROLANG_PROCESS_H

#include <QObject>

#include "base/types.h"
#include "core/connection.h"
#include "core/process_geometry.h"

namespace core {

class Process : public QObject {
  Q_OBJECT
  using BaseClass = QObject;

public:
  using Function = std::function<void(Process*)>;
  explicit Process(const base::String& name,
                   bool atomic,
                   Process* parent_process = nullptr,
                   QObject* parent = nullptr);
  virtual ~Process();

  base::String getName() const { return name_; }

  void addInput(const base::String& name,
                WireType type);
  void addOutput(const base::String& name,
                 WireType type);

  Connection* getInput(base::WireIndex index) const;
  Connection* getOutput(base::WireIndex index) const;

  base::Vector<Connection*> getInputs() const { return inputs_; }
  base::Vector<Connection*> getOutputs() const { return outputs_; }

  void setFunction(const Function& f) { function_ = f; }

  void calculate();

  bool isAtomic() const { return is_atomic_; }

  ProcessGeometry* getGeometry() { return geometry_; }
  const ProcessGeometry* getGeometry() const { return geometry_; }

  Process* getParentProcess() const { return parent_process_; }

private slots:
  void tryToCalculate();

private:
  void addConnection(Connection::EMode mode,
                     const base::String& name,
                     WireType type);

private:
  ProcessGeometry* geometry_ {};
  Process* parent_process_ {};
  bool is_atomic_ {false};
  base::Vector<Connection*> inputs_;
  base::Vector<Connection*> outputs_;
  base::String name_ {};
  Function function_;
};

} // namespace core

#endif // PROLANG_PROCESS_H
