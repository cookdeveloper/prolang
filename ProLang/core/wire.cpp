#include "core/wire.h"

namespace core {

Wire::Wire(WireType type,
           QObject* parent)
  : QObject(parent),
    type_(type) {
  qDebug() << "Wire::Wire";
  geometry_ = new WireGeometry(this);
}


void Wire::send(const base::WireValue& value) {
  assert(value.isValid());
  value_ = value;
  emit activated();
}


base::WireValue Wire::get() const {
  assert(value_.isValid());
  return value_;
}

} // namespace core
