#ifndef PROLANG_GEOMETRY_H
#define PROLANG_GEOMETRY_H

#include <QObject>
#include <QRectF>

namespace core {

class Geometry : public QObject {
  Q_OBJECT
  using BaseClass = QObject;
public:
  explicit Geometry(QObject* parent = nullptr);
  virtual ~Geometry();

  QRectF getBoundingRect() const { return bounding_rect_; }

protected:
  void setBoundingRect(const QRectF& rect) { bounding_rect_ = rect; }

private:
  QRectF bounding_rect_ {};
};

} // namespace core

#endif // PROLANG_GEOMETRY_H
