#include "core/process_geometry.h"

namespace core {

ProcessGeometry::ProcessGeometry(QObject* parent)
  : Geometry(parent) {

}


ProcessGeometry::~ProcessGeometry() {}


void ProcessGeometry::setRect(const QRectF& rect) {
  rect_ = rect;
  setBoundingRect(rect);
}

} // namespace core
