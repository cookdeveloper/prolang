QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    base/strings.cpp \
    core/connection.cpp \
    core/functions.cpp \
    core/geometry.cpp \
    core/process.cpp \
    core/process_geometry.cpp \
    core/processes.cpp \
    core/wire.cpp \
    core/wire_geometry.cpp \
    gui/canvas.cpp \
    gui/main_window.cpp \
    main.cpp

HEADERS += \
    base/constants.h \
    base/strings.h \
    base/types.h \
    core/connection.h \
    core/functions.h \
    core/geometry.h \
    core/process.h \
    core/process_geometry.h \
    core/processes.h \
    core/wire.h \
    core/wire_geometry.h \
    core/wire_types.h \
    gui/canvas.h \
    gui/main_window.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
